#!/bin/bash

# Stop unpredictible behavior
set -o errexit          # Exit on most errors
set -o nounset          # Disallow expansion of unset variables
set -o pipefail         # Use last non-zero exit code in a pipeline

ansible-pull -U https://gitlab.com/odiroot/dev-env --ask-become-pass

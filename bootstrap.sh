#!/bin/bash

# Stop unpredictible behavior
set -o errexit          # Exit on most errors
set -o nounset          # Disallow expansion of unset variables
set -o pipefail         # Use last non-zero exit code in a pipeline

sudo apt-add-repository -y ppa:ansible/ansible
sudo apt update
sudo apt install -y ansible git